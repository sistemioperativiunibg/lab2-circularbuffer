package it.unibg.so.lab2;

public class Main {

	public static void main(String[] args) {
		Buffer<Integer> buffer = new CircularBuffer(2);
		
		Thread t = new Task(buffer);
		t.start();
		
		buffer.push(1);
		buffer.push(2);
		buffer.push(3);
		
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println(buffer.toString());
	}

}

class Task extends Thread{
	
	private Buffer<Integer> buffer;
	
	public Task(Buffer<Integer> buffer){
		this.buffer = buffer;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		buffer.pop();
	}
	
}