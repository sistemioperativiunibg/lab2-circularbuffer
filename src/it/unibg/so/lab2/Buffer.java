package it.unibg.so.lab2;

public interface Buffer<T> {
	public void push(T item);
	public T pop();
	public boolean isEmpty();
}
