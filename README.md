# CIRCULAR BUFFER EXAMPLE #

Un *buffer circolare* viene inizializzato dando in input la lunghezza desiderata.

Operazione di `push` (inserimento di un elemento):

* il primo inserimento parte nella posizione 0, poi 1, 2, e così via
* quando il buffer è pieno, l’inserimento torna ad essere fatto dalla posizione 0
* l'inserimento di un elemento in una posizione già occupata da un altro elemento, causa il blocco del thread fino a che la posizione non viene liberata

Operazione di `pop` (rimozione di un elemento):

* l’operazione rimuove l’elemento più vecchio contenuto nel buffer
* la rimozione di un elemento in un *buffer* è vuoto causa il blocco del thread fino a che un altro elemento non viene inserito

Operazione `isEmpty`:

* ritorna true se il *buffer* è vuoto, false altrimenti

Usando gli opportuni meccanismi di sincronizzazione (`synchronized`, `wait`, `notify`) implementare la classe `CircularBuffer` che implementa le operazioni richieste. Tutte le operazioni eseguite sulla struttura dati da più thread concorrenti devono avvenire in mutua esclusione.